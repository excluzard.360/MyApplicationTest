package com.example.oxygen9.myapplication.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.oxygen9.myapplication.Datum;
import com.example.oxygen9.myapplication.R;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>  {
    private static final String TAG = MyAdapter.class.getName();

    private Context mContext;
    private ViewHolder mViewHolder;
    private List<Datum> mDatas;

    private static OnClickListener mOnClickListener;

    public MyAdapter(Context mContext, List<Datum> data, OnClickListener mOnClickListener) {
        this.mContext = mContext;
        this.mDatas = data;

//        if(this.mOnClickListener==null){
//            this.mOnClickListener = new OnClickListener() {
//                @Override
//                public void onClick(Datum data) {
//                    Log.d(TAG, "onClick() called with: data = [" + data + "]");
//                }
//            };
//        }else{
            this.mOnClickListener = mOnClickListener;
//        }



    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.title.setText(mDatas.get(position).getDocType());

        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnClickListener.onClick(mDatas.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.mDatas==null?0:this.mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ViewGroup itemLayout;
        private TextView title;


        public ViewHolder(View itemView) {
            super(itemView);

            this.itemLayout = itemView.findViewById(R.id.item);
            this.title = itemView.findViewById(R.id.title);


        }

    }

    public interface OnClickListener{
        public void onClick(Datum data);
    }

}
