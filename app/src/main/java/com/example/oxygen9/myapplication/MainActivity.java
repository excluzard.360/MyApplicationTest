package com.example.oxygen9.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.oxygen9.myapplication.adapter.MyAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    private ViewHolder mViewHolder;
    private RecyclerView.Adapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        Example json = getJson();
        getSupportActionBar().setTitle("id:"+json.getId()+" "+" Name:"+json.getFirstName()+" "+" "+json.getLastName());
        adapter = new MyAdapter(MainActivity.this, json.getData(), new MyAdapter.OnClickListener() {
            @Override
            public void onClick(Datum data) {
                Log.d(TAG, "onClick() called with: data = [" + data.getDescription().getEn() + "]");
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(MainActivity.this);
                builder.setMessage(data.getDescription().getTh()+"/"+data.getDescription().getEn());

                builder.show();
            }
        });


        mViewHolder.listView.setAdapter(adapter);
    }

    private void initView(){
        if(mViewHolder==null){
            mViewHolder = new ViewHolder();
        }
    }

    Example getJson(){
        String json = "{\n" +
                "  \"Id\": \"045687\",\n" +
                "  \"firstName\": \"Patamon\",\n" +
                "  \"lastName\": \"luksame\",\n" +
                "  \"data\": [\n" +
                "    {\n" +
                "      \"docType\": \"A1\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"เอกสารที่ใช้แสดงตน A1\",\n" +
                "        \"en\": \"Document TYPE A1\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"A2\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"เอกสารที่ใช้แสดงตน A2\",\n" +
                "        \"en\": \"Document TYPE A2\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"A3\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"เอกสารที่ใช้แสดงตน A3\",\n" +
                "        \"en\": \"Document TYPE A3\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"A4\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"เอกสารที่ใช้แสดงตน A4\",\n" +
                "        \"en\": \"Document TYPE A4\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"B1\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"หน้าสมุดบัญชีธนาคารรับชำระเงินปันผล B1\",\n" +
                "        \"en\": \"BookBank B1\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"B2\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"หน้าสมุดบัญชีธนาคารรับชำระเงินปันผล B2\",\n" +
                "        \"en\": \"BookBank B2\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"C1\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"ข้อมูลของผู้ขอเอาประกันภัย C1\",\n" +
                "        \"en\": \"Information TYPE C1\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"C2\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"ข้อมูลของผู้ขอเอาประกันภัย C2\",\n" +
                "        \"en\": \"Information TYPE C2\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"C3\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"ข้อมูลของผู้ขอเอาประกันภัย C3\",\n" +
                "        \"en\": \"Information TYPE C3\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"C4\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"ข้อมูลของผู้ขอเอาประกันภัย C4\",\n" +
                "        \"en\": \"Information TYPE C4\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"D1\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"อื่นๆ D1\",\n" +
                "        \"en\": \"Other D1\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"D2\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"อื่นๆ D2\",\n" +
                "        \"en\": \"Other D2\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"D3\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"อื่นๆ D3\",\n" +
                "        \"en\": \"Other D3\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"D4\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"อื่นๆ D4\",\n" +
                "        \"en\": \"Other D4\"\n" +
                "      }\n" +
                "    },\n" +
                "    {\n" +
                "      \"docType\": \"D5\",\n" +
                "      \"description\": {\n" +
                "        \"th\": \"อื่นๆ D5\",\n" +
                "        \"en\": \"Other D5\"\n" +
                "      }\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        Example example = new Gson().fromJson(json, Example.class);
//        Log.d(TAG, example.toString());
//        Log.d(TAG, example.getData().get(0).getDocType());

        return example;
    }

    class ViewHolder{
        RecyclerView listView;

        public ViewHolder() {
            this.listView = findViewById(R.id.list_view);
            this.listView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

        }
    }


}
